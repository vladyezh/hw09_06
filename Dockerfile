FROM centos:7

RUN rpmkeys --import file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 && \
    yum -y update && \
    yum install -y python3 python3-pip curl && \
    yum clean all 
RUN pip3 install flask flask_restful flask_jsonpify
WORKDIR /python_api
ADD /repo/python-api.py /python_api/python-api.py
ENTRYPOINT ["python3", "/python_api/python-api.py"]
